local actions = require'telescope.actions'
require('telescope').setup{
    defaults = {
	vimgrep_arguments = {
	'rg',
	'--color=never',
	'--no-heading',
	'--with-filename',
	'--line-number',
	'--column',
	'--smart-case'
	},
	prompt_prefix = "> ",
	selection_caret = "> ",
	entry_prefix = "  ",
	initial_mode = "insert",
	selection_strategy = "reset",
	sorting_strategy = "descending",
	layout_strategy = "horizontal",
	layout_config = {
	    preview_cutoff = 120,
	    width = 0.75,
	    prompt_position = "bottom",
	    horizontal = {
		mirror = false,
	    },
	    vertical = {
		mirror = false,
	    },
	},
	file_sorter =  require'telescope.sorters'.get_fuzzy_file,
	file_ignore_patterns = {},
	generic_sorter =  require'telescope.sorters'.get_generic_fuzzy_sorter,
	winblend = 0,
	mappings = {
	    i = {
		["<c-k>"] = actions.move_selection_previous,
		["<c-j>"] = actions.move_selection_next,
		["<tab>"] = actions.add_selection,
		["<esc>"] = actions.close,
	    },
	},
	border = {},
	borderchars = { '─', '│', '─', '│', '╭', '╮', '╯', '╰' },
	color_devicons = true,
	use_less = true,
	set_env = { ['COLORTERM'] = 'truecolor' }, -- default = nil,
	file_previewer = require'telescope.previewers'.vim_buffer_cat.new,
	grep_previewer = require'telescope.previewers'.vim_buffer_vimgrep.new,
	qflist_previewer = require'telescope.previewers'.vim_buffer_qflist.new,
	buffer_previewer_maker = require'telescope.previewers'.buffer_previewer_maker
    },
    pickers = {
	buffers = {
	    theme = "dropdown",
	    previewer = false,
	},
    },
    extensions = {
	fzf = {
	    fuzzy = true,                    -- false will only do exact matching
	    override_generic_sorter = false, -- override the generic sorter
	    override_file_sorter = true,     -- override the file sorter
	    case_mode = "smart_case",        -- or "ignore_case" or "respect_case"
	}
    }
}
require('telescope').load_extension('fzf')
