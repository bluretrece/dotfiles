require('material').setup()
vim.g.material_style = "darker"
vim.g.nimda_use_gui_italics= 0
vim.g.tokyonight_italic_comments = 0
vim.g.tokyonight_italic_keywords = 0
vim.g.tokyonight_style = "night"
vim.g.solarized_italics = 0
-- vim.cmd[[colorscheme jellybeans-nvim]]
-- vim.cmd[[colorscheme doom-one]]
-- vim.cmd[[colorscheme tokyonight]]
local catppuccin = require("catppuccin")

-- configure it
catppuccin.setup(
    {
		transparent_background = false,
		term_colors = false,
		styles = {
			comments = "NONE",
			functions = "NONE",
			keywords = "NONE",
			strings = "NONE",
			variables = "NONE",
		},
		integrations = {
			treesitter = true,
			native_lsp = {
				enabled = true,
				virtual_text = {
					errors = "italic",
					hints = "italic",
					warnings = "italic",
					information = "italic",
				},
				underlines = {
					errors = "underline",
					hints = "underline",
					warnings = "underline",
					information = "underline",
				},
			},
			lsp_trouble = false,
			lsp_saga = false,
			gitgutter = false,
			gitsigns = false,
			telescope = false,
			nvimtree = {
				enabled = false,
				show_root = false,
			},
			which_key = false,
			indent_blankline = {
				enabled = false,
				colored_indent_levels = false,
			},
			dashboard = false,
			neogit = false,
			vim_sneak = false,
			fern = false,
			barbar = false,
			bufferline = false,
			markdown = false,
			lightspeed = false,
			ts_rainbow = false,
			hop = false,
		},
	}
)
vim.cmd[[colorscheme catppuccin]]
-- vim.cmd[[colorscheme nimda]]
